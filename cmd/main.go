package main

import (
	"log"

	"gitlab.com/medium_project/api_service/api"
	_ "gitlab.com/medium_project/api_service/api/docs"
	"gitlab.com/medium_project/api_service/config"
	grpcPkg "gitlab.com/medium_project/api_service/pkg/grpc_client"
	"gitlab.com/medium_project/api_service/pkg/logger"
)

func main() {
	cfg := config.Load(".")

	grpcConn, err := grpcPkg.New(cfg)
	if err != nil {
		log.Fatalf("failed to get grpc connettion: %v", err)
	}

	logger := logger.New()

	apiServer := api.New(&api.RouterOptions{
		Cfg:        &cfg,
		GrpcClient: grpcConn,
		Logger:     logger,
	})

	err = apiServer.Run(cfg.HttpPort)
	if err != nil {
		log.Fatalf("failed to run server: %v", err)
	}
}
