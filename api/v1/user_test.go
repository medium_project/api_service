package v1_test

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/bxcodec/faker/v4"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/medium_project/api_service/api/models"
	pbu "gitlab.com/medium_project/api_service/genproto/user_service"
	"gitlab.com/medium_project/api_service/pkg/grpc_client/mock_grpc"
)

func TestGetAllUsers(t *testing.T) {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/v1/users", nil)
	router.ServeHTTP(resp, req)

	assert.Equal(t, 200, resp.Code)

	fmt.Println(resp.Body.String())
}

func createUser(t *testing.T) *models.User {
	authSuperadmin := loginSuperadmin(t)

	resp := httptest.NewRecorder()

	payload, err := json.Marshal(models.CreateUserRequest{
		FirstName:       faker.FirstName(),
		LastName:        faker.LastName(),
		PhoneNumber:     faker.Phonenumber(),
		Email:           faker.Email(),
		Gender:          "male",
		UserName:        faker.Username(),
		ProfileImageUrl: faker.URL(),
		Type:            "user",
		Password:        "ASsd@132",
	})
	assert.NoError(t, err)

	req, _ := http.NewRequest("POST", "/v1/users", bytes.NewBuffer(payload))
	req.Header.Add("Authorization", authSuperadmin.AccessToken)
	router.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusCreated, resp.Code)

	body, _ := io.ReadAll(resp.Body)

	var response models.User

	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)

	return &response
}

func deleteUser(t *testing.T, id int64) {
	authSuperadmin := loginSuperadmin(t)

	resp := httptest.NewRecorder()

	url := fmt.Sprintf("/v1/users/%d", id)

	req, _ := http.NewRequest("DELETE", url, nil)
	req.Header.Add("Authorization", authSuperadmin.AccessToken)
	router.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusOK, resp.Code)
}
func TestCreateUser(t *testing.T) {
	user := createUser(t)
	deleteUser(t, user.ID)
}

func TestCreateUserMock(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	reqBody := models.CreateUserRequest{
		FirstName: faker.FirstName(),
		LastName:  faker.LastName(),
		Email:     faker.Email(),
		Password:  "asdf123",
		Type:      "superadmin",
		Gender:    "male",
	}

	userService := mock_grpc.NewMockUserServiceClient(ctrl)
	userService.EXPECT().Create(context.Background(), &pbu.User{
		FirstName: reqBody.FirstName,
		LastName:  reqBody.LastName,
		Email:     reqBody.Email,
		Password:  reqBody.Password,
		Type:      reqBody.Type,
		Gender:    reqBody.Gender,
	}).Times(1).Return(&pbu.User{
		Id:        1,
		FirstName: reqBody.FirstName,
		LastName:  reqBody.LastName,
		Email:     reqBody.Email,
		Type:      reqBody.Type,
		Gender:    reqBody.Gender,
		CreatedAt: time.Now().Format(time.RFC3339),
	}, nil)

	payload, err := json.Marshal(reqBody)
	assert.NoError(t, err)

	grpcConn.SetUserService(userService)

	accessToken := mockAuthMiddleware(t, ctrl, "user", "create")

	req, _ := http.NewRequest("POST", "/v1/users", bytes.NewBuffer(payload))
	req.Header.Add("Authorization", accessToken)

	rec := httptest.NewRecorder()
	router.ServeHTTP(rec, req)

	assert.Equal(t, http.StatusCreated, rec.Code)

	body, _ := io.ReadAll(rec.Body)

	var response models.User

	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)
	assert.Equal(t, reqBody.FirstName, response.FirstName)
	assert.Equal(t, reqBody.Email, response.Email)
}

func TestUpdateUserMock(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	reqBody := models.UpdateUserRequest{
		FirstName: faker.FirstName(),
		LastName: faker.LastName(),
		PhoneNumber: faker.Phonenumber(),
		Gender: "male",
		UserName: faker.Username(),
		ProfileImageUrl: faker.URL(),
	}

	userService := mock_grpc.NewMockUserServiceClient(ctrl)
	userService.EXPECT().Update(context.Background(), &pbu.User{
		Id: 1,
		FirstName: reqBody.FirstName,
		LastName: reqBody.LastName,
		PhoneNumber: reqBody.PhoneNumber,
		Gender: reqBody.Gender,
		Username: reqBody.UserName,
		ProfileImageUrl: reqBody.ProfileImageUrl,
	}).Times(1).Return(&pbu.User{
		Id: 1,
		FirstName: reqBody.FirstName,
		LastName: reqBody.LastName,
		PhoneNumber: reqBody.PhoneNumber,
		Gender: reqBody.Gender,
		Username: reqBody.UserName,
		ProfileImageUrl: reqBody.ProfileImageUrl,
	}, nil)

	payload, err := json.Marshal(reqBody)
	assert.NoError(t, err)

	grpcConn.SetUserService(userService)

	accessToken := mockAuthMiddleware(t, ctrl, "users", "update")

	req, _ := http.NewRequest("PUT", "/v1/users/1", bytes.NewBuffer(payload))
	req.Header.Add("Authorization", accessToken)

	rec := httptest.NewRecorder()
	router.ServeHTTP(rec, req)

	assert.Equal(t, http.StatusOK, rec.Code)

	body, _ := io.ReadAll(rec.Body)

	var response models.User

	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)
	assert.Equal(t, reqBody.FirstName, response.FirstName)
}

func TestGetUserMock(t *testing.T) {
	ctrl := gomock.NewController(t)
	ctrl.Finish()

	respBody := pbu.User{ 
		FirstName: faker.FirstName(), 
		LastName: faker.LastName(), 
		PhoneNumber: faker.Phonenumber(), 
		Email: faker.Email(), 
		Gender: "male", 
		Username: faker.Username(), 
		ProfileImageUrl: faker.URL(), 
		Type: "superadmin", 
		CreatedAt: time.Now().Format(time.RFC3339), 
	}

	userService := mock_grpc.NewMockUserServiceClient(ctrl)
	userService.EXPECT().Get(context.Background(), &pbu.IdRequest{
		Id: 1,
	}).Times(1).Return(&pbu.User{
		Id: 1,
		FirstName: respBody.FirstName,
		LastName: respBody.LastName,
		PhoneNumber: respBody.PhoneNumber,
		Email: respBody.Email,
		Gender: respBody.Gender,
		Username: respBody.Username,
		ProfileImageUrl: respBody.ProfileImageUrl,
		Type: respBody.Type,
		CreatedAt: respBody.CreatedAt,
	}, nil)

	grpcConn.SetUserService(userService)

	req, _ := http.NewRequest("GET", "/v1/users/1", nil)

	rec := httptest.NewRecorder()
	router.ServeHTTP(rec, req)

	assert.Equal(t, http.StatusOK, rec.Code)

	body, _ := io.ReadAll(rec.Body)

	var response models.User

	err := json.Unmarshal(body, &response)
	assert.NoError(t, err)
	assert.NotEmpty(t, response.FirstName)
}

func TestGetUser(t *testing.T) {
	user := createUser(t)

	resp := httptest.NewRecorder()

	url := fmt.Sprintf("/v1/users/%d", user.ID)
	req, _ := http.NewRequest("GET", url, nil)

	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code)

	body, _ := io.ReadAll(resp.Body)

	var response models.User

	err := json.Unmarshal(body, &response)
	assert.NoError(t, err)

	deleteUser(t, user.ID)
}

func TestGetAllUsersCases(t *testing.T) {
	testCases := []struct {
		name          string
		query         string
		checkResponse func(t *testing.T, recoder *httptest.ResponseRecorder)
	}{
		{
			name:  "success case",
			query: "?limit=10&page=1",
			checkResponse: func(t *testing.T, response *httptest.ResponseRecorder) {
				assert.Equal(t, http.StatusOK, response.Code)
			},
		},
		{
			name:  "incorrect limit param",
			query: "?limit=ads&page=1",
			checkResponse: func(t *testing.T, response *httptest.ResponseRecorder) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			resp := httptest.NewRecorder()
			url := fmt.Sprintf("/v1/users%s", tc.query)

			req, _ := http.NewRequest("GET", url, nil)
			router.ServeHTTP(resp, req)

			tc.checkResponse(t, resp)
		})
	}
}

func TestUpdateUser(t *testing.T) {
	authSuperadmin := loginSuperadmin(t)
	user := createUser(t)
	resp := httptest.NewRecorder()

	url := fmt.Sprintf("/v1/users/%d", user.ID)

	payload, err := json.Marshal(models.UpdateUserRequest{
		FirstName:       faker.FirstName(),
		LastName:        faker.LastName(),
		PhoneNumber:     faker.Phonenumber(),
		Gender:          "male",
		UserName:        faker.Username(),
		ProfileImageUrl: faker.URL(),
	})
	assert.NoError(t, err)

	req, _ := http.NewRequest("PUT", url, bytes.NewBuffer(payload))
	req.Header.Add("Authorization", authSuperadmin.AccessToken)
	router.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusOK, resp.Code)

	body, _ := io.ReadAll(resp.Body)

	var response models.User

	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)
}

func TestDeleteUser(t *testing.T) {
	user := createUser(t)
	deleteUser(t, user.ID)
}
