package v1_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.com/medium_project/api_service/api/models"
)

func createPost(t *testing.T) *models.Post {
	authUser := loginUser(t)
	category := createCategory(t)

	resp := httptest.NewRecorder()

	payload, err := json.Marshal(models.Post{
		Title:       faker.Sentence(),
		Description: faker.Sentence(),
		ImageUrl:    faker.URL(),
		UserID:      authUser.Id,
		CategoryID:  category.ID,
	})
	assert.NoError(t, err)

	req, _ := http.NewRequest("POST", "/v1/posts", bytes.NewBuffer(payload))
	req.Header.Add("Authorization", authUser.AccessToken)
	router.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusCreated, resp.Code)

	body, _ := io.ReadAll(resp.Body)

	var response models.Post

	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)

	return &response
}

func deletePost(t *testing.T, id int64) {
	authUser := loginUser(t)

	resp := httptest.NewRecorder()

	url := fmt.Sprintf("/v1/posts/%d", id)

	req, _ := http.NewRequest("DELETE", url, nil)
	req.Header.Add("Authorization", authUser.AccessToken)
	router.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusOK, resp.Code)
}

func TestCreatePost(t *testing.T) {
	post := createPost(t)
	deletePost(t, post.ID)
}

func TestUpdatePost(t *testing.T) {
	authUser := loginUser(t)
	post := createPost(t)
	resp := httptest.NewRecorder()

	url := fmt.Sprintf("/v1/posts/%d", post.ID)

	payload, err := json.Marshal(models.UpdatePostRequest{
		Title: faker.Word(),
		Description: faker.Sentence(),
		ImageUrl: faker.URL(),
		UserID: authUser.Id,
		CategoryID: post.CategoryID,
		ViewsCount: 1,
	})
	assert.NoError(t, err)

	req, _ := http.NewRequest("PUT", url, bytes.NewBuffer(payload))
	req.Header.Add("Authorization", authUser.AccessToken)
	router.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusOK, resp.Code)

	body, _ := io.ReadAll(resp.Body)

	var response models.Category

	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)

	deletePost(t, post.ID)
}

func TestGetPost(t *testing.T) {
	post := createPost(t)

	resp := httptest.NewRecorder()

	url := fmt.Sprintf("/v1/posts/%d", post.ID)
	req, _ := http.NewRequest("GET", url, nil)

	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code)

	body, _ := io.ReadAll(resp.Body)

	var response models.Post

	err := json.Unmarshal(body, &response)
	assert.NoError(t, err)

	deletePost(t, post.ID)
}

func TestGetAllPostsCases(t *testing.T) {
	testCases := []struct {
		name          string
		query         string
		checkResponse func(t *testing.T, recoder *httptest.ResponseRecorder)
	}{
		{
			name:  "success case",
			query: "?limit=10&page=1",
			checkResponse: func(t *testing.T, response *httptest.ResponseRecorder) {
				assert.Equal(t, http.StatusOK, response.Code)
			},
		},
		{
			name:  "incorrect limit param",
			query: "?limit=ads&page=1",
			checkResponse: func(t *testing.T, response *httptest.ResponseRecorder) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			resp := httptest.NewRecorder()
			url := fmt.Sprintf("/v1/posts%s", tc.query)

			req, _ := http.NewRequest("GET", url, nil)
			router.ServeHTTP(resp, req)

			tc.checkResponse(t, resp)
		})
	}
}

func TestDeletePost(t *testing.T) {
	post := createPost(t)
	deletePost(t, post.ID)
}