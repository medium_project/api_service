package v1_test

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	pbu "gitlab.com/medium_project/api_service/genproto/user_service"

	"github.com/bxcodec/faker/v4"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/medium_project/api_service/api/models"
	"gitlab.com/medium_project/api_service/pkg/grpc_client/mock_grpc"
)

func loginUser(t *testing.T) *models.AuthResponse {
	resp := httptest.NewRecorder()

	payload, err := json.Marshal(models.LoginRequest{
		Email:    "testuser@gmail.com",
		Password: "User@123",
	})
	assert.NoError(t, err)

	req, _ := http.NewRequest("POST", "/v1/auth/login", bytes.NewBuffer(payload))
	router.ServeHTTP(resp, req)

	assert.Equal(t, 200, resp.Code)

	body, _ := io.ReadAll(resp.Body)

	var response models.AuthResponse

	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)

	return &response
}

func loginSuperadmin(t *testing.T) *models.AuthResponse {
	resp := httptest.NewRecorder()

	payload, err := json.Marshal(models.LoginRequest{
		Email:    "ahrorahrorovnt@gmail.com",
		Password: "Ahror@4991",
	})
	assert.NoError(t, err)

	req, _ := http.NewRequest("POST", "/v1/auth/login", bytes.NewBuffer(payload))
	router.ServeHTTP(resp, req)

	assert.Equal(t, 200, resp.Code)

	body, _ := io.ReadAll(resp.Body)

	var response models.AuthResponse

	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)

	return &response
}

func TestLogin(t *testing.T) {
	loginUser(t)
	loginSuperadmin(t)
}

func TestRegisterUser(t *testing.T) {
	testCases := []struct {
		name         string
		body         models.RegisterRequest
		checkRequest func(t *testing.T, recoder *httptest.ResponseRecorder)
	}{
		{
			name: "bad request wrong email adress",
			body: models.RegisterRequest{
				FirstName: faker.FirstName(),
				LastName:  faker.LastName(),
				Email:     "asdflkjasldfkjadsf",
				Password:  "Abc@1234",
			},
			checkRequest: func(t *testing.T, response *httptest.ResponseRecorder) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
			},
		},
		{
			name: "bad request wrong password",
			body: models.RegisterRequest{
				FirstName: faker.FirstName(),
				LastName:  faker.LastName(),
				Email:     faker.Email(),
				Password:  "112313121",
			},
			checkRequest: func(t *testing.T, response *httptest.ResponseRecorder) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
			},
		},
		{
			name: "bad request email already exists",
			body: models.RegisterRequest{
				FirstName: faker.FirstName(),
				LastName:  faker.LastName(),
				Email:     "testuser@gmail.com",
				Password:  "Abcd@123",
			},
			checkRequest: func(t *testing.T, response *httptest.ResponseRecorder) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
			},
		},
		{
			name: "success case",
			body: models.RegisterRequest{
				FirstName: faker.FirstName(),
				LastName:  faker.LastName(),
				Email:     faker.Email(),
				Password:  "Abcd@123",
			},
			checkRequest: func(t *testing.T, response *httptest.ResponseRecorder) {
				assert.Equal(t, http.StatusOK, response.Code)
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			resp := httptest.NewRecorder()
			payload, err := json.Marshal(tc.body)
			assert.NoError(t, err)
			req, _ := http.NewRequest("POST", "/v1/auth/register", bytes.NewBuffer(payload))
			router.ServeHTTP(resp, req)
			tc.checkRequest(t, resp)
		})
	}
}

func TestLoginMokc(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	reqBody := models.LoginRequest{
		Email:    "mocktest@gmail.com",
		Password: "asd132",
	}

	authService := mock_grpc.NewMockAuthServiceClient(ctrl)
	authService.EXPECT().Login(context.Background(), &pbu.LoginRequest{
		Email:    reqBody.Email,
		Password: reqBody.Password,
	}).Times(1).Return(&pbu.AuthResponse{
		Id:          1,
		FirstName:   "Sardor",
		LastName:    "Sattorov",
		Email:       "mocktest@gmail.com",
		Type:        "superadmin",
		CreatedAt:   time.Now().Format(time.RFC3339),
		AccessToken: faker.Sentence(),
	}, nil)

	payload, err := json.Marshal(reqBody)
	assert.NoError(t, err)

	grpcConn.SetAuthService(authService)

	req, _ := http.NewRequest("POST", "/v1/auth/login", bytes.NewBuffer(payload))
	rec := httptest.NewRecorder()
	router.ServeHTTP(rec, req)

	assert.Equal(t, 200, rec.Code)

	body, _ := io.ReadAll(rec.Body)

	var response models.AuthResponse
	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)
	assert.NotEmpty(t, response.AccessToken)
}


func mockAuthMiddleware(t *testing.T, ctrl *gomock.Controller, resource, action string) string {
	accessToken := faker.UUIDHyphenated()

	authService := mock_grpc.NewMockAuthServiceClient(ctrl)
	authService.EXPECT().VerifyToken(context.Background(), &pbu.VerifyTokenRequest{
		AccessToken: accessToken,
		Resource: resource,
		Action: action,
	}).Times(1).Return(&pbu.AuthPayload{
		Id: faker.UUIDHyphenated(),
		UserId: 1,
		Email: faker.Email(),
		UserType: "superadmin",
		HasPermission: true,
		IssuedAt: time.Now().Format(time.RFC3339),
		ExpiredAt: time.Now().Add(time.Hour).Format(time.RFC3339),
	}, nil)

	grpcConn.SetAuthService(authService)

	return accessToken
}
