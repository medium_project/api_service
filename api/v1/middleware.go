package v1

import (
	"context"
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/medium_project/api_service/genproto/user_service"
)

type Payload struct {
	Id        string `json:"id"`
	UserID    int64  `json:"user_id"`
	Email     string `json:"email"`
	UserType  string `json:"user_type"`
	IssuedAt  string `json:"issued_at"`
	ExpiredAt string `json:"expired_at"`
}

const (
	authorizationHeaderKey  = "authorization"
	authorizationPayloadKey = "authorization_payload"
)

func (h *handlerV1) AuthMiddleWare(resource, action string) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		accessToken := ctx.GetHeader(authorizationHeaderKey)

		if len(accessToken) == 0 {
			err := errors.New("authorization header is not provided")
			h.logger.Error(err)
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, errResponse(err))
			return
		}
		payload, err := h.grpcClient.AuthService().VerifyToken(context.Background(), &user_service.VerifyTokenRequest{
			AccessToken: accessToken,
			Resource:    resource,
			Action:      action,
		})

		if err != nil {
			h.logger.WithError(err).Error("failed to verify token")
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, errResponse(err))
			return
		}
		if !payload.HasPermission {
			ctx.AbortWithStatusJSON(http.StatusForbidden, errResponse(ErrNotAllowed))
		}

		ctx.Set(authorizationPayloadKey, Payload{
			Id:        payload.Id,
			UserID:    payload.UserId,
			Email:     payload.Email,
			UserType:  payload.UserType,
			IssuedAt:  payload.IssuedAt,
			ExpiredAt: payload.ExpiredAt,
		})
		ctx.Next()
	}
}

func (h *handlerV1) GetAuthPayload(ctx *gin.Context) (*Payload, error) {
	i, exist := ctx.Get(authorizationPayloadKey)
	if !exist {
		return nil, errors.New("payload not found")
	}
	payload, ok := i.(Payload) // error is here
	if !ok {
		return nil, errors.New("unknown user")
	}
	return &payload, nil
}
