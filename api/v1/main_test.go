package v1_test

import (
	"log"
	"os"
	"testing"

	"github.com/gin-gonic/gin"
	"gitlab.com/medium_project/api_service/api"
	"gitlab.com/medium_project/api_service/config"
	grpcPkg "gitlab.com/medium_project/api_service/pkg/grpc_client"
	"gitlab.com/medium_project/api_service/pkg/logger"
)

var (
	router *gin.Engine
	grpcConn grpcPkg.GrpcClientI
)

func TestMain(m *testing.M) {
	var err error
	cfg := config.Load("./../..")

	logrus := logger.New()

	grpcConn, err = grpcPkg.New(cfg)
	if err != nil {
		log.Fatalf("failed to get grpc connections: %v", err)
	}

	ginEngine := api.New(&api.RouterOptions{
		Cfg:        &cfg,
		GrpcClient: grpcConn,
		Logger:     logrus,
	})

	router = ginEngine
	os.Exit(m.Run())
}
