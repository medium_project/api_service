package v1_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.com/medium_project/api_service/api/models"
)

func createCategory(t *testing.T) *models.Category {
	authSuperadmin := loginSuperadmin(t)

	resp := httptest.NewRecorder()

	payload, err := json.Marshal(models.Category{
		Title: faker.Word(),
	})
	assert.NoError(t, err)

	req, _ := http.NewRequest("POST", "/v1/categories", bytes.NewBuffer(payload))
	req.Header.Add("Authorization", authSuperadmin.AccessToken)
	router.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusCreated, resp.Code)

	body, _ := io.ReadAll(resp.Body)

	var response models.Category

	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)

	return &response
}


func deleteCategory(t *testing.T, id int64) {
	authSuperadmin := loginSuperadmin(t)

	resp := httptest.NewRecorder()

	url := fmt.Sprintf("/v1/categories/%d", id)

	req, _ := http.NewRequest("DELETE", url, nil)
	req.Header.Add("Authorization", authSuperadmin.AccessToken)
	router.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusOK, resp.Code)
}

func TestCreateCategory(t *testing.T) {
	createCategory(t)
}

func TestUpdateCategory(t *testing.T) {
	authSuperadmin := loginSuperadmin(t)
	category := createCategory(t)
	resp := httptest.NewRecorder()

	url := fmt.Sprintf("/v1/categories/%d", category.ID)

	payload, err := json.Marshal(models.CreateCategoryRequest{
		Title: faker.Word(),
	})
	assert.NoError(t, err)

	req, _ := http.NewRequest("PUT", url, bytes.NewBuffer(payload))
	req.Header.Add("Authorization", authSuperadmin.AccessToken)
	router.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusOK, resp.Code)

	body, _ := io.ReadAll(resp.Body)

	var response models.Category

	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)

	deleteCategory(t, category.ID)
}


func TestGetCategory(t *testing.T) {
	user := createCategory(t)

	resp := httptest.NewRecorder()

	url := fmt.Sprintf("/v1/categories/%d", user.ID)
	req, _ := http.NewRequest("GET", url, nil)

	router.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code)

	body, _ := io.ReadAll(resp.Body)

	var response models.Category

	err := json.Unmarshal(body, &response)
	assert.NoError(t, err)

	deleteCategory(t, user.ID)
}


func TestGetAllCategoriesCases(t *testing.T) {
	testCases := []struct {
		name          string
		query         string
		checkResponse func(t *testing.T, recoder *httptest.ResponseRecorder)
	}{
		{
			name:  "success case",
			query: "?limit=10&page=1",
			checkResponse: func(t *testing.T, response *httptest.ResponseRecorder) {
				assert.Equal(t, http.StatusOK, response.Code)
			},
		},
		{
			name:  "incorrect limit param",
			query: "?limit=ads&page=1",
			checkResponse: func(t *testing.T, response *httptest.ResponseRecorder) {
				assert.Equal(t, http.StatusBadRequest, response.Code)
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			resp := httptest.NewRecorder()
			url := fmt.Sprintf("/v1/categories%s", tc.query)

			req, _ := http.NewRequest("GET", url, nil)
			router.ServeHTTP(resp, req)

			tc.checkResponse(t, resp)
		})
	}
}


func TestDeleteCategory(t *testing.T) {
	category := createCategory(t)
	deleteCategory(t, category.ID)
}
